package dev.creepah.fac;

import com.gmail.nossr50.datatypes.player.McMMOPlayer;
import com.gmail.nossr50.util.player.UserManager;
import com.massivecraft.factions.Rel;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.MPlayer;
import com.massivecraft.factions.entity.MPlayerColl;
import com.wimbli.WorldBorder.BorderData;
import com.wimbli.WorldBorder.WorldBorder;
import dev.creepah.chat.ChatCore;
import dev.creepah.chat.factionschat.ChatType;
import dev.creepah.chat.factionschat.FactionsChat;
import dev.creepah.fac.cmd.ChatToggleCommand;
import dev.creepah.fac.cmd.GunpowderCommand;
import dev.creepah.fac.cmd.PotionStackCommand;
import dev.creepah.fac.cmd.SpawnSetCommand;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import tech.rayline.core.plugin.RedemptivePlugin;
import tech.rayline.core.plugin.UsesFormats;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@UsesFormats
public final class FBasics extends RedemptivePlugin implements Listener {

    private static FBasics instance;
    public static FBasics get() {
        return instance;
    }

    public List<UUID> chatOff = new ArrayList<>();
    public List<Material> materials = new ArrayList<>();

    @Override
    protected void onModuleEnable() throws Exception {
        instance = this;

        registerListener(this);

        loadMaterialList();

        registerCommand(new ChatToggleCommand());
        registerCommand(new PotionStackCommand());
        registerCommand(new SpawnSetCommand());
        registerCommand(new GunpowderCommand());
    }

    @Override
    protected void onModuleDisable() throws Exception {
        materials.clear();
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onTeleport(PlayerTeleportEvent event) {
        Player player = event.getPlayer();
        Block block = player.getLocation().getBlock();

        if (!getConfig().getBoolean("cancel-water-teleport")) return;

        if (block.getWorld().getEnvironment().equals(World.Environment.NETHER)) {
            if (event.getTo().getY() >= 127) {
                event.setCancelled(true);
                return;
            }
        }

        if (event.getCause() == PlayerTeleportEvent.TeleportCause.ENDER_PEARL
                || event.getCause() == PlayerTeleportEvent.TeleportCause.UNKNOWN) return;

        if (event.getCause() == PlayerTeleportEvent.TeleportCause.PLUGIN) {
            BorderData worldBorder = WorldBorder.plugin.getWorldBorder(player.getWorld().getName());
            if (block.isLiquid() || block.getType().equals(Material.STATIONARY_WATER) || block.getType().equals(Material.STATIONARY_LAVA)) {
                if (worldBorder == null) return;
                if (worldBorder.insideBorder(player.getLocation())) {
                    event.setCancelled(true);
                    player.sendMessage(formatAt("teleport-cancelled").get());
                }
            }
        }
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        if (event.getTo().getWorld().getEnvironment().equals(World.Environment.NETHER)) {
            if (!getConfig().getBoolean("nether-prevent")) return;
            if (event.getTo().getY() >= 127) {
                String[] spawn = getConfig().getString("spawn").split("_");
                Location s = new Location(Bukkit.getWorld(spawn[3]), Double.parseDouble(spawn[0]), Double.parseDouble(spawn[1]), Double.parseDouble(spawn[2]));

                event.getPlayer().teleport(s);
                event.getPlayer().sendMessage(formatAt("nether-attempt")
                        .with("x", (int) event.getFrom().getX())
                        .with("y", (int) event.getFrom().getY())
                        .with("z", (int) event.getFrom().getZ())
                        .get());
            }
        }
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        if (chatOff.contains(event.getPlayer().getUniqueId())) chatOff.remove(event.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Set<Player> receivers = event.getRecipients();
        Player pl = event.getPlayer();

        McMMOPlayer p = UserManager.getPlayer(pl);
        MPlayer player = MPlayer.get(event.getPlayer());

        List<MPlayer> cOff = new ArrayList<>();
        List<McMMOPlayer> chatO = new ArrayList<>();

        for (UUID uuid : chatOff) {
            cOff.add(MPlayerColl.get().get(uuid));
            chatO.add(UserManager.getPlayer(Bukkit.getPlayer(uuid)));
        }

        List<Player> nonReceivers = new ArrayList<>();
        for (MPlayer fp : cOff) {
            Faction fac = fp.getFaction();
            Faction f = player.getFaction();
            FactionsChat chat = ChatCore.get().getFactionsChat();

            if (fac == null || f == null)
                continue;
            if (fac.equals(f) && chat.getChatType(player.getPlayer()) == ChatType.PUBLIC)
                continue;
            if (fac.getRelationTo(f).isAtLeast(Rel.ALLY) && chat.getChatType(player.getPlayer()) == ChatType.ALLY)
                continue;
            if (fac.getRelationTo(f).isAtLeast(Rel.TRUCE) && chat.getChatType(player.getPlayer()) == ChatType.TRUCE)
                continue;
            if (!nonReceivers.contains(fp.getPlayer()))
                nonReceivers.add(fp.getPlayer());
        }

        for (McMMOPlayer mc : chatO) {
            if (mc.getParty() == null || p.getParty() == null)
                continue;
            if (mc.getParty().equals(p.getParty()) && mc.isChatEnabled(com.gmail.nossr50.datatypes.chat.ChatMode.PARTY))
                continue;
            if (!nonReceivers.contains(mc.getPlayer()))
                nonReceivers.add(mc.getPlayer());
        }

        receivers.removeAll(nonReceivers);
        nonReceivers.clear();
    }

    public void loadMaterialList() {
        List<String> str = getConfig().getStringList("materials");
        for (String s : str) {
            if (Material.valueOf(s.toUpperCase()) != null) materials.add(Material.valueOf(s.toUpperCase()));
        }
    }

    @EventHandler
    public void onDrop(ItemSpawnEvent event) {
        if (!getConfig().getBoolean("chest-fix")) return;
        if (event.getEntityType() == EntityType.DROPPED_ITEM) {
            if (materials.contains(event.getLocation().getBlock().getType())) {
                event.getLocation().add(0, 1, 0).getBlock().setType(event.getEntity().getItemStack().getType());
                event.getEntity().remove();
            }
        }
    }

}
