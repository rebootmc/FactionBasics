package dev.creepah.fac.cmd;

import com.gmail.nossr50.config.skills.alchemy.PotionConfig;
import com.gmail.nossr50.datatypes.skills.alchemy.AlchemyPotion;
import dev.creepah.fac.FBasics;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandMeta;
import tech.rayline.core.command.CommandPermission;
import tech.rayline.core.command.RDCommand;

import java.util.HashMap;
import java.util.Map;

@CommandPermission("facbasics.pot")
@CommandMeta(description = "Stack your potions")
public class PotionStackCommand extends RDCommand {

    public PotionStackCommand() {
        super("pot");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        stackFor(player);
    }

    public void stackFor(Player player) {
        Inventory inv = player.getInventory();
        Map<AlchemyPotion, Integer> mcmmo = new HashMap<>();
        Map<Short, Integer> normal = new HashMap<>();

        if (!FBasics.get().getConfig().getBoolean("potion-stack")) return;

        ItemStack[] c = inv.getContents();
        for (ItemStack stack : c) {
            if (stack != null && stack.getType() == Material.POTION) {
                Short du = stack.getDurability();
                AlchemyPotion a = PotionConfig.getInstance().getPotion(du);
                if (a != null) {
                    if (mcmmo.containsKey(a)) mcmmo.replace(a, mcmmo.get(a) + stack.getAmount());
                    else mcmmo.put(a, stack.getAmount());
                    inv.remove(stack);
                } else {
                    if (normal.containsKey(du)) normal.replace(du, normal.get(du) + stack.getAmount());
                    else normal.put(du, stack.getAmount());
                    inv.remove(stack);
                }
            }
        }

        int maxStack = FBasics.get().getConfig().getInt("max-stack");

        for (AlchemyPotion ap : mcmmo.keySet()) {
            if (mcmmo.get(ap) > maxStack) {
                int timesOver = (int) Math.floor(mcmmo.get(ap) / maxStack);
                for (int i = 0; i < timesOver; i++) inv.addItem(ap.toItemStack(maxStack));
                int leftOver = mcmmo.get(ap) - (timesOver * maxStack);
                if (leftOver > 0) inv.addItem(ap.toItemStack(leftOver));
            } else inv.addItem(ap.toItemStack(mcmmo.get(ap)));
        }

        for (Short s : normal.keySet()) {
            if (normal.get(s) > maxStack) {
                int timesOver = (int) Math.floor(normal.get(s) / maxStack);
                for (int i = 0; i < timesOver; i++) inv.addItem(new ItemStack(Material.POTION, maxStack, s));
                int leftOver = normal.get(s) - (timesOver * maxStack);
                if (leftOver > 0) inv.addItem(new ItemStack(Material.POTION, leftOver, s));
            } else inv.addItem(new ItemStack(Material.POTION, normal.get(s), s));
        }

        normal.clear();
        mcmmo.clear();
        player.sendMessage(FBasics.get().formatAt("potions-stacked").get());
    }
}
