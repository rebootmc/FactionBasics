package dev.creepah.fac.cmd;

import dev.creepah.fac.FBasics;
import org.bukkit.entity.Player;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandMeta;
import tech.rayline.core.command.CommandPermission;
import tech.rayline.core.command.RDCommand;

import java.util.List;
import java.util.UUID;

@CommandPermission("facbasics.global")
@CommandMeta(description = "Enable/Disable normal chat")
public class ChatToggleCommand extends RDCommand {

    public ChatToggleCommand() {
        super("global");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        List<UUID> co = FBasics.get().chatOff;
        if (co.contains(player.getUniqueId())) {
            co.remove(player.getUniqueId());
            player.sendMessage(FBasics.get().formatAt("enabled-global").get());
        } else {
            co.add(player.getUniqueId());
            player.sendMessage(FBasics.get().formatAt("disabled-global").get());
        }
    }
}
