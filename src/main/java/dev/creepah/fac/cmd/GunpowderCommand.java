package dev.creepah.fac.cmd;

import dev.creepah.fac.FBasics;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandMeta;
import tech.rayline.core.command.CommandPermission;
import tech.rayline.core.command.RDCommand;

@CommandPermission("facbasics.gp")
@CommandMeta(description = "Convert gunpowder into TNT", aliases = {"gunpowder"})
public class GunpowderCommand extends RDCommand {

    public GunpowderCommand() {
        super("gp");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        PlayerInventory inv = player.getInventory();
        int g = FBasics.get().getConfig().getInt("gunpowder", 5);

        int tnt = (getGunpowderAmount(player) / g);
        int r = tnt * g;

        if (tnt > 0) {
            int removed = 0;
            for (ItemStack i : inv.getContents()) {
                if (i != null && i.getType() == Material.SULPHUR) {
                    removed += i.getAmount();
                    inv.remove(i);
                }
            }

            inv.addItem(new ItemStack(Material.TNT, tnt));
            inv.addItem(new ItemStack(Material.SULPHUR, removed - r));
        }

        player.updateInventory();
        player.sendMessage(FBasics.get().formatAt("tnt-converted").withModifier("gunpowder", r).withModifier("tnt", tnt).get());
    }

    public int getGunpowderAmount(Player player) {
        int amount = 0;
        for (ItemStack item : player.getInventory().getContents()) {
            if (item != null && item.getType() == Material.SULPHUR) amount += item.getAmount();
        }

        return amount;
    }
}
