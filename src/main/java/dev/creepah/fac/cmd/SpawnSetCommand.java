package dev.creepah.fac.cmd;

import dev.creepah.fac.FBasics;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandPermission;
import tech.rayline.core.command.RDCommand;

@CommandPermission("facbasics.spawnset")
public class SpawnSetCommand extends RDCommand {

    public SpawnSetCommand() {
        super("spawnset");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        Location loc = player.getLocation();
        FBasics.get().getConfig()
                .set("spawn", (int) loc.getX() + "_" + (int) loc.getY() + "_" + (int) loc.getZ() + "_" + loc.getWorld().getName());
        player.sendMessage(FBasics.get().formatAt("anti-nether-spawn-set").get());
        FBasics.get().saveConfig();
    }
}
